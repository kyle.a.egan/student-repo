## Return to Day 2 Exercises
* [Day 2 ](../../README.md)


# Exercise 5 - Scoped Configuration Management

[Table of Contents](#table-of-contents)
- [Step 1 - Persist](#step-1-persist)
- [Step 2 - Deploy](#step-2-deploy)
- [Step 3 - Detect](#step-3-detect)
- [Step 4 - Remediate](#step-4-remediate)

## Solution
If needed.
[solution](solution/)

## Objective

Create `job-templates` that use playbooks developed with `validated content` for networks. The main use case is to establish a single `source of truth` for your network environment. The validated content will allow you to gather facts from brownfield devices to simplify creating a single source of truth mapped to the `inventory host_vars`. With a SSOT in mind, we can easily manage `configuration drift` and accept or remediate changes using validated content for networks.

## Overview

The Ansible validated content for networks `network.base` focuses on abstract platform-agnostic network automation and enhances the experience of resource module consumption by providing production-ready content. This network base content acts as the core to the other network validated content, such as `network.ospf` and `network.interfaces`.   

### Step 1 - Persist
The resource_manager role uses the `persist action` to get the facts for a given resource and store it as inventory host_vars. 

1. Create a job-template named `Network-Validated-Persist` with the following parameters. Please note that we will reuse the gitlab token created back in exercise 1. Hence this job-template will have two credentials assigned. The machine credential "Workshop Credential" and the Gitlab Student credential type with the "Gitlab Credential".

#### Paremeters:
- name: **Network-Validated-Persist**
- organization: **Red Hat network organization**
- inventory: **Workshop Inventory**
- project: **Student Project**
- playbook: **day2/2-systematic/5-scoped-config-management/persist.yml**
- credentials:
  - "Workshop Credential"
  - "Gitlab Credential"
- execution_environment: "Validated Network"

2. Review the `persist.yml` playbook located `~/student-repo/day2/2-systematic/5-scoped-config-management/persist.yml`
- Notice how the gitlab variables are defined in `vars/git.yml` and the Gitlab Credential is used to securely store the token. Secondly the resource modules are listed in the `vars/resource` for the play. In turn, the resource_manager role will render a hostvars/.yml file or each resource entry. For this exercise in particular, we are using shell commands to commit and pull directly to the main branch of the gitlab project "student-repo". You may notice the lint tool complaining about not using the ansible.scm module instead of git commands. As you recall, the SCM module was used in back in day2 Exercise one for backing up configs to the gitlab repo. In that exercise is was ok because ansible.scm pushes updates to a new branch and the timestamp would indicate that batch of backups. Considering we aren't interested in making new branches as we will save changes directly to the main branch, we'll kindly tell lint to mind its own dang business...

3. Launch the `Network-Validated-Persist` job template and review the output.

4. Verify the host_vars entries for each of the four routers in the `day2/2-systematic/5-scoped-config-mangement/host_vars/` of your gitlab student-repo project.

- day2/2-systematic/5-scoped-config-mangement/host_vars/
 ![hostvars](../../images/hostvars_persist.png)  


### Step 2 - Deploy
The resource_manager role uses the Deploy action to apply changes to the listed resources. The deploy action works well for additions to the device's configuration because it uses the state of merged. The state of merged will not overwrite any existing configurations.

1. Add a new BGP network prefix "192.168.4.4/32 entry to the "~/student-repo/day2/2-systematic/5-scoped-config-mangement/hostvars/rtr2/bgp_address_family.yaml" configuration file. This entry will advertise the Loopback0 from rtr4.

2. Complete the git steps for your change. You must save, commit the file in the VSCode IDE and "sync" push to gitlab after fixing the file.
![Save](../../images/save_commit.png)

or update from the terminal
~~~
git add --all
git commit -m "deploy"
git push
~~~

3. Create a new playbook `deploy.yml` in `~/student-repo/day2/2-systematic/5-scoped-config-management/` 
- Note you can use persist.yml as a reference but this time the action is "deploy". Hint, save time in this playbook by listing only the bgp_address_family resource instead of all the resource modules. If needed, you can take a peak in the solutions folder.

4. Create the Network-Validate-Deploy job-template with the following parameters.
Hint, you can copy the Network-Validated-Persist job-template and slighlty modify.

* Caution, Whenever you create a net-new playbook, you must sync the "Student Project" to have access to the new deploy.yml playbook.

Parameters:
- name: **Network-Validated-Deploy**
- organization: **Red Hat network organization**
- inventory: **Workshop Inventory**
- project: **Student Project**
- playbook: **day2/2-systematic/5-scoped-config-management/deploy.yml**
- credentials: **Workshop Credential**
- execution_environment: **Validated Network**

5. Launch the Network-Validated-Deploy job-template
Ensure that the playbook "changes" adds the network prefix "192.168.4.4 to rtr2 
       
### Step 3 - Detect
The resource_manager role uses the detect action to detect configuration drift in the configured resources of the network devices.

1. Access rtr1 from the VSCode terminal and ssh

~~~
ssh rtr1
~~~

2. Use the CLI to add a network prefix to rtr1 for rtr3's loobback0 that is a mistake. Sometimes OOB changes made from the CLI are prown to mistakes. Ooops you just fat fingered it....

~~~
config t
router bgp 65000
address-family ipv4 
network 192.168.1.3 255.255.255.255
end
~~~
3. Create a new playbook named detect.yml. 
Feel free to copy deploy.yml. Simply keep the resource bgp_address_family and change the action to detect. If needed, review the solution folder.

* Caution, you must sync the `Student Project` to have access to the new playbook `deploy.yml`!

4. Create a job-template named `Network-Validatd-Detect` with the following parameters. 
- Feel free to copy a previous jobtemplate to save time.
* Paremeters:
- name: **Network-Validated-Detect**
- organization: **Red Hat network organization**
- inventory: **Workshop Inventory**
- project: **Student Project**
- playbook: **day2/2-systematic/5-scoped-config-management/detect.yml**
- credentials:
  - **Workshop Credential**
- execution_environment: **Validated Network**

5. Launch the `Network-Validated-Detect` and note the configuraiton drift for rtr1.

### Step 4 - Remediate
The resource_manager role uses the remediation action to overrite (add or remove) configuration that are not reconsiled with the host_vars yaml files. In this exercise the host_vars files are our single source of truth (SSOT).

1. edit and save the student-repo/host_vars/rtr1/bgp_address_family.yaml 
Add an entry for rtr3's loopback0 ip address "192.168.3.3"

~~~
bgp_address_family:
    address_family:
    -   afi: ipv4
        neighbors:
        -   activate: true
            neighbor_address: 10.200.200.2
        networks:
        -   address: 10.100.100.0
            mask: 255.255.255.0
        -   address: 10.200.200.0
            mask: 255.255.255.0
        -   address: 172.16.0.0
        -   address: 192.168.1.1
            mask: 255.255.255.255
        -   address: 192.168.1.1
            mask: 255.255.255.255
        -   address: 192.168.3.3
            mask: 255.255.255.255   
        redistribute:
        -   ospf:
                process_id: 1
    as_number: '65000'
~~~

2. Complete the git steps for your change. You must save, commit the file in the VSCode IDE and "sync" push to gitlab after making the change to the hostvar file above.
![Save](../../images/save_commit.png)

or update from the terminal
~~~
git add --all
git commit -m "remediate"
git push
~~~

3. Create a new playbook `remediate.yml` in `~/student/day2/2-systematic/5-scoped-config-management/` 
- Note you can use `detect.yml` as a reference but this time the action is "remediate". Hint, save time by listing only the bgp_address_family resource. If needed, you can take a peak in the solutions folder.

4. Create the `Network-Validate-Remediate` job-template with the following parameters.
Hint, you can copy the Network-Validated-Detect job-template and slighlty modify.

* Caution, you must sync the `Student Project` to have access to the new playbook `remediate.yml`!

Parameters:
- name: **Network-Validated-Remediate**
- organization: **Red Hat network organization**
- inventory: **Workshop Inventory**
- project: **Student Project**
- playbook: **day2/2-systematic/5-scoped-config-management/remediate.yml**
- credentials: **Workshop Credential**
- execution_environment: **Validated Network**

5. Launch the `Network-Validated-Remediate` job-template
Ensure that the playbook removes the "errored" 192.168.1.3 network prefix mistake to rtr1 and "changes" adds the network prefix "192.168.3.3 to rtr1. 

### If needed a setup.yml with associated playbooks and hostvars are already available in  ~/student-repo/day2/2-systematic/5-scoped-config-management/solution
       
Congratulations Validated content has reconciled your devices with the source of truth!  

## Next Exercise
* [ 6-automated-netops-validation-capstone](../../3-institutionalized/6-automated-netops-validation/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)


